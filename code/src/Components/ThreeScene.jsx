//Libraries imports
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import * as THREE from 'three';
import FBXLoader from 'three-fbx-loader';
import cls from 'classnames';
import TWEEN from '@tweenjs/tween.js';

//Assets imports
import phonesMesh from '../Assets/meshes/headphones-test.fbx';
import headMesh from '../Assets/meshes/basehead.fbx';
import phonesTexture from '../Assets/textures/compressed4x.png';

const OrbitControls = require('three-orbit-controls')(THREE)

class ThreeScene extends Component {

    constructor(props){
        super(props);

        this.width = 0;
        this.height = 0;
        this.THREE = THREE;
        this.leftStartAngle = 180;   //  => 155  --
        this.rightStartAngle = 0;    //  => 25   ++
        this.leftCurrentAngle = 180;
        this.rightCurrentAngle = 0;
        this.maxAngle = 25;
        this.lastState = '';
        this.lastProp = false;
        this.animationState = 'stopped';
        this.hasLoaded = false;
        this.leds = [];
        this.optionSelected = true;
        this.index = -1;
        this.previousIndex = 0;
        this.lastProp = {
            data: {
                active: null
            }
        }
        this.cameraAngles = [
            { x: -6.6, y: 27.3, z: 9.5 },
            { x: 11.45, y: 1, z: -13.0 },
            { x: 0.27, y: 20, z: -17.8 }
        ]
        this.state = {
            style: null,
            timeout: true,
            mouse: {
                x: 0,
                y: 0
            }
        }
    }

    componentDidMount() {
        this.width = this.mount.clientWidth;
        this.height = this.mount.clientHeight;

        //ADD SCENE
        this.scene = new THREE.Scene();

        //ADD CAMERA
        // this.camera = new THREE.OrthographicCamera(-this.width/0.5, this.width/0.5, this.height/0.5, -this.height/0.5, 0.1, 3000);
        this.camera = new THREE.PerspectiveCamera(75, this.width / this.height, 0.1, 10000);
        this.camera.position.set(this.cameraAngles[1].x, this.cameraAngles[1].y, this.cameraAngles[1].z);
        this.mesh = null;

        let canvas = document.getElementById('canvas');
        
        canvas.width  = window.innerWidth; 
        canvas.height = window.innerHeight;

        this.renderer = new THREE.WebGLRenderer({ canvas: canvas, antialias: true, alpha: true });

        //ADD RENDERER
        this.renderer.setClearColor('#333', 0);
        this.renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);
        this.renderer.gammaOutput = true;
        this.renderer.gammaFactor = 1.5;
        this.mount.appendChild(this.renderer.domElement);

        this.light = new THREE.DirectionalLight('#fff', 0.5);
        this.light.position.set(20, -20, -20);
        this.scene.add(new THREE.AmbientLight('#fff'));
        this.scene.add(this.light);

        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.enableZoom = true;
        this.controls.maxDistance = 35;
        this.controls.minDistance = 20;
        this.controls.enablePan = false;

        this.textLoader = new THREE.TextureLoader();
        this.texture = this.textLoader.load(phonesTexture);
        this.phonesMaterial = new THREE.MeshPhongMaterial({ map: this.texture });

        let manager = new THREE.LoadingManager();
        manager.onProgress =  (url, itemsLoaded, itemsTotal) => {
            let loadingState = { itemsLoaded, itemsTotal };
            this.props.loading(loadingState);
        };

        this.loader = new FBXLoader(manager);

        this.loader.load(phonesMesh, (obj) => {

            this.mesh = obj;

            this.mesh.position.set(0, 0, 0);
            this.mesh.traverse(object => { object.material = this.phonesMaterial });

            this.findLeds();

			let phonesHelper = new THREE.BoxHelper(this.mesh);
			this.phonesMiddle = new THREE.Vector3();
			phonesHelper.geometry.computeBoundingBox();
			
			this.phonesMiddle.x = (phonesHelper.geometry.boundingBox.max.x + phonesHelper.geometry.boundingBox.min.x) / 2;
			this.phonesMiddle.y = (phonesHelper.geometry.boundingBox.max.y + phonesHelper.geometry.boundingBox.min.y) / 2;
            this.phonesMiddle.z = (phonesHelper.geometry.boundingBox.max.z + phonesHelper.geometry.boundingBox.min.z) / 2;
            
            let headHelper = new THREE.BoxHelper(this.head);
			this.headMiddle = new THREE.Vector3();
			headHelper.geometry.computeBoundingBox();
			
			this.headMiddle.x = (headHelper.geometry.boundingBox.max.x + headHelper.geometry.boundingBox.min.x) / 2;
			this.headMiddle.y = (phonesHelper.geometry.boundingBox.max.y + headHelper.geometry.boundingBox.min.y) / 2;
			this.headMiddle.z = (headHelper.geometry.boundingBox.max.z + headHelper.geometry.boundingBox.min.z) / 2;

			this.controls.target = this.phonesMiddle;
			this.controls.maxPolarAngle = Math.PI/1.5;

			this.light.castShadow = true;
			this.light.target = this.mesh;

			this.mesh.receiveShadow = true;
            this.mesh.castShadow = true;

			this.scene.add(this.mesh);
			this.scene.add(this.light.target);

            this.controls.update();
            this.hasLoaded = true;
        });

        this.loader.load(headMesh, (obj) => {
            this.head = obj.children[0];

            this.head.scale.set(4.5, 4.5, 4.5);
            this.head.position.set(0, 8, 0);

            let headMat = new THREE.MeshLambertMaterial({ color:0xB6132F, transparent: true, opacity: 0 });

            this.head.material = headMat;

            this.wireframe = this.head.clone();
            this.wireframe.position.set(0, 8, 0);
            this.wireframe.material = new THREE.MeshBasicMaterial({ wireframe: true, transparent: true, opacity: 0 });

            this.head.rotation.set(0, 0, 0);

            this.scene.add(this.head);
            this.scene.add(this.wireframe);
        });

        window.addEventListener("resize", this.resize.bind(this));
        this.start();
    }

    componentWillUnmount() {
        this.stop();
        this.mount.removeChild(this.renderer.domElement);
        window.removeEventListener("resize", this.resize.bind(this));
    }

    degToRad(degrees) {
        return (degrees * (Math.PI / 180));
    }

    readProps() {
        if(this.props.data.active !== this.lastProp.data.active && this.props.data.active) {
            this.changeState('running__open');
        } else if (this.props.data.active !== this.lastProp.data.active && !this.props.data.active) {
            this.changeState('running__close');
        }
        this.lastProp = this.props;
    }

    changeState(state) {
        if(this.animationState !== 'stopped') this.lastState = this.animationState;
        this.animationState = state;
    }

    openSpeakers() {
        if(this.leftCurrentAngle > this.leftStartAngle - this.maxAngle) {
            if(this.mesh) {
                let positionFrom = {
                    x: this.phonesMiddle.x,
                    y: this.phonesMiddle.y,
                    z: this.phonesMiddle.z
                }
                
                let detailsPosition = {
                    x: 25.722210705312513,
                    y: 6.854690365732421,
                    z: -0.21601672941860395
                }

                let currentPosition = this.camera.position;
                let currentRotation = {
                    y: this.head.rotation.y
                }

                this.headTween = new TWEEN.Tween(positionFrom)
                .to(this.headMiddle, 500)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate(() => {
                    let newVector = new THREE.Vector3();
                    newVector.x = positionFrom.x
                    newVector.y = positionFrom.y
                    newVector.z = positionFrom.z
                    this.controls.target = newVector;
                    this.controls.update();
                })
                this.headTween.start();

                this.resetTween = new TWEEN.Tween(currentRotation)
                .to({y: 0}, 1000)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate(() => {
                    this.mesh.rotation.y = currentRotation.y;
                    this.head.rotation.y = currentRotation.y;
                    this.wireframe.rotation.y = currentRotation.y;
                })
                this.resetTween.start();

                this.cameraTween = new TWEEN.Tween(currentPosition)
                .to(detailsPosition, 1000)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate(() => {
                    this.camera.position.set(
                        currentPosition.x,
                        currentPosition.y,
                        currentPosition.z
                    )
                    this.controls.update();
                })
                this.cameraTween.start();

                this.leftCurrentAngle -= 1;
                this.rightCurrentAngle += 1;

                this.head.material.opacity += 0.045;
                this.wireframe.material.opacity += 0.045;

                this.mesh.children.map(part => {
                    if(part.name === 'left_speakerModel')
                        part.rotation.z -= this.degToRad(1); //180 to 155
                    else if (part.name === 'right_speakerModel')
                        part.rotation.z += this.degToRad(1); //0 to 25

                    return null;
                });
            }
        } else {
            this.changeState('stopped');
        }
    }

    closeSpeakers() {
        if(this.leftCurrentAngle < this.leftStartAngle) {
            if(this.mesh) {
                let positionFrom = {
                    x: this.headMiddle.x,
                    y: this.headMiddle.y,
                    z: this.headMiddle.z
                }
                this.phonesTween = new TWEEN.Tween(positionFrom)
                .to(this.phonesMiddle, 500)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate(() => {
                    let newVector = new THREE.Vector3();
                    newVector.x = positionFrom.x
                    newVector.y = positionFrom.y
                    newVector.z = positionFrom.z
                    this.controls.target = newVector;
                    this.controls.update();
                })
                this.phonesTween.start();

                this.leftCurrentAngle += 1;
                this.rightCurrentAngle -= 1;

                this.head.material.opacity -= 0.045;
                this.wireframe.material.opacity -= 0.045;

                this.mesh.children.map(part => { 
                    if(part.name === 'left_speakerModel')
                        part.rotation.z += this.degToRad(1); //155 to 180
                    else if (part.name === 'right_speakerModel')
                        part.rotation.z -= this.degToRad(1); // 25 to 0

                    return null;
                });
            }
        } else {
            this.changeState('stopped');
        }
    }

    findLeds() {
        let redLed = new THREE.MeshLambertMaterial({ color: 0xb6132f });
        let disabledLed = new THREE.MeshLambertMaterial({ color: 0x636365 });

        this.leds = this.mesh.children[0].children;
        this.leds.forEach(el => {
            if(el.name === 'led_1Model') el.material = disabledLed;
            if(el.name === 'led_2Model') el.material = redLed;
            if(el.name === 'led_3Model') el.material = disabledLed;
            if(el.name === 'led_4Model') el.material = disabledLed;
        })
    }

    trackLeds() {
        if(this.leds.length && this.props.data.activeButton) {
            let vector = new THREE.Vector3();
            
            // TODO: need to update this when resize window
            let widthHalf = 0.5 * this.renderer.context.canvas.width;
            let heightHalf = 0.5 * this.renderer.context.canvas.height;

            let selectedWave;

            if(this.props.data.activeButton === 'alpha') selectedWave = this.leds[1];
            if(this.props.data.activeButton === 'beta') selectedWave = this.leds[2];
            if(this.props.data.activeButton === 'delta') selectedWave = this.leds[3];
            if(this.props.data.activeButton === 'theta') selectedWave = this.leds[0];

            
            selectedWave.updateMatrixWorld();
            vector.setFromMatrixPosition(selectedWave.matrixWorld);
            vector.project(this.camera);
            
            vector.x = ( vector.x * widthHalf ) + widthHalf;
            vector.y = - ( vector.y * heightHalf ) + heightHalf;
            
            this.drawOver(vector)
        }
    }

    drawOver(point) {

        this.setState(prevState => ({
            style: {
                ...prevState.style,
                // transform: `rotate(${salopeInDegrees}deg)`,
                // borderBottom: 'dashed 0.1vw #666',
                position: 'absolute',
                border: '0.15vw dashed #B6132F',
                width: '1vw',
                height: '1vw',
                top: point.y,
                left: point.x,
                transform: 'translate(-50%, -50%) rotate(45deg)',
                zIndex: '9999999'
            }
        }))
    }

    async changeCamera() {
        this.previousIndex = this.index > 0 ? this.index : 0
        if(this.index < this.cameraAngles.length-1) {
            this.index++;
        } else {
            this.index = 0
        }

        let positionFrom = {
            x: this.cameraAngles[this.previousIndex].x,
            y: this.cameraAngles[this.previousIndex].y,
            z: this.cameraAngles[this.previousIndex].z
        }

        let positionTo = {
            x: this.cameraAngles[this.index].x,
            y: this.cameraAngles[this.index].y,
            z: this.cameraAngles[this.index].z
        }

        this.cameraTween = new TWEEN.Tween(positionFrom)
        .to(positionTo, this.index === 1 ? 4000 : 2000)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onUpdate(() => {
            if(this.props.data.overlay) {
                this.camera.position.set(
                    positionFrom.x,
                    positionFrom.y,
                    positionFrom.z
                );
                this.controls.update();
            }
        })

        this.cameraTween.start();
		
		if(this.loading !== null && !this.loading) {
			this.setState( { timeout: await this.wait() })
		}
	}

	wait() {
		return new Promise(resolve => {
			setTimeout(() => { resolve(true) }, 6000);
		});
	}

    resize() {
        let canvas = document.getElementById('canvas');
        canvas.height = window.innerHeight;
        canvas.width  = window.innerWidth;

        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
    }

    start = () => {
        if (!this.frameId) {
            this.frameId = requestAnimationFrame(this.animate);
        }
    }

    stop = () => {
        cancelAnimationFrame(this.frameId);
    }

    animate = () => {
        this.readProps();
        if(this.hasLoaded && !this.props.data.active) {
            this.mesh.rotation.y += 0.001;
            this.head.rotation.y += 0.001;
            this.wireframe.rotation.y += 0.001;
        }

        if(this.props.data.overlay){
            this.controls.enableRotate = false;
            this.controls.enableZoom = false;
        }

        if(this.state.timeout && this.props.data.overlay) {
            this.setState({timeout: false}, () => {
                this.changeCamera();
            })
        } else if(!this.props.data.overlay) {
            this.controls.enableRotate = true;
            this.controls.enableZoom = true;
        }

        if(this.animationState === 'running__open') this.openSpeakers();
        if(this.animationState === 'running__close') this.closeSpeakers();
        this.trackLeds();
        this.renderScene();
        this.frameId = window.requestAnimationFrame(this.animate);
        TWEEN.update();
    }

    renderScene = () => {
        this.renderer.render(this.scene, this.camera);
    }

    render() {
        return(
            <div id="canvasContainer" ref={(mount) => { this.mount = mount }}>
                <canvas id="canvas" className={cls({active: this.props.data.window}, {no_overlay: !this.props.data.overlay})}/>
                <div className="track">
                    {/* <div className={cls("highlight", {active: this.props.data.window})} style={this.state.style}></div> */}
                </div>
            </div>
        )
    }
}

ThreeScene.protoTypes = {
	loading : PropTypes.func
};

export default ThreeScene