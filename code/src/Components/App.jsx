//Libraries imports
import React, { Component } from 'react';
import ThreeScene from './ThreeScene';
import Interface from './Interface';
import LoadingOverlay from './LoadingOverlay'
import '../Assets/styles/main.scss';

class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            UIData: {
                overlay: true,
                active: false
            },
            loadingData: null
        }
    }

    loadingState(params) {
        this.setState({ loadingData : params })
    }

    interfaceState(params) {
        this.setState({ UIData : params })
    }

    render() {
        return(
            <div id="canvasContainer" ref={(mount) => { this.mount = mount }}>
                <LoadingOverlay data={this.state.loadingData}/>
                <ThreeScene loading={this.loadingState.bind(this)} data={this.state.UIData}/>
                <Interface callback={this.interfaceState.bind(this)}/>
            </div>
        )
    }
}

export default App