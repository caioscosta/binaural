import React, { Component } from 'react';
import cls from 'classnames';

class LoadingOverlay extends Component {

	constructor(props){
		super(props);

		this.percentage = null;
		this.loading = null;
		

		this.state = { timeout: false };
	}

	async calcPercentage() {
		let loadData = this.props.data;
		if(loadData){
			this.percentage = loadData.itemsLoaded / loadData.itemsTotal * 100;
			this.loading = this.props.data.itemsLoaded !== this.props.data.itemsTotal;
	
		}
		if(this.loading !== null && !this.loading) {
			this.setState( { timeout: await this.wait() })
		}
	}

	wait() {
		return new Promise(resolve => {
			setTimeout(() => { resolve(true) }, 2000);
		});
	}

	render() {

		if(!this.state.timeout) this.calcPercentage();
		
		return(
			<div className={cls("overlay", {loading: !this.state.timeout})}>
				<div className="loading_container">
					<div className="icon_wrapper">
						<div className="loading_icon">
							<div/><div/>
						</div>
					</div>
					<div className="loading_progress-bar">
						<div className="loading_outterBar">
							<div className="loading_innerBar" style={{width: `${this.percentage}%`}}/>
						</div>
						<div className="loading_progress">{this.percentage ? this.percentage + "%" : "0%"}</div>
					</div>
				</div>
			</div>
		)
	}
}
export default LoadingOverlay