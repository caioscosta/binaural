import React, { Component } from 'react';
import cls from 'classnames'
import PropTypes from 'prop-types';

import data from '../Assets/data/features.js';
import track1 from '../Assets/media/alpha.mp3';
import track2 from '../Assets/media/beta.mp3';
import track3 from '../Assets/media/delta.mp3';
import track4 from '../Assets/media/theta.mp3';

class Interface extends Component {

	constructor(props){
		super(props);
		this.state = {
			active: false, 
			overlay: true, 
			window: false,
			title: '',
			description: '',
			activeButton: null,
			playing: false,
			timer: "0:00",
			sliderValue: 0,
			searching: false,
			searchValue: 0,
			tutorial: false,
			tutorialStep: 1,
			showTutorial: true,
			minimized: false
		};
		this.audio = new Audio();
		this.update = this.update.bind(this)
	}

	executeCallback() {
		let data = {
			overlay: this.state.overlay,
			active: this.state.active,
			window: this.state.window,
			activeButton: this.state.activeButton,
			isMobile: this.isMobile()
		}
		this.props.callback(data);
	}

	toggleDetail() {
		this.setState( {active: !this.state.active, window: false, activeButton: null, playing: false, minimized: false}, () => {
			if(this.audio) {
				this.audio.pause();
				this.audio.currentTime = 0;
			}
			this.executeCallback();
		})
	}

	toggleOverlay() {
		this.setState( {overlay: !this.state.overlay, active: false}, () => {
			if(this.state.showTutorial) this.setState({ tutorial: true, showTutorial: false });
			this.executeCallback();
		});
	}

	toggleWindow() {
		this.setState( {window: !this.state.window, activeButton: null}, () => {
			this.executeCallback();
		});
	}

	openWindow(param) {
		if(!this.isMobile()){
			this.setState( {window: true, activeButton: param}, () => {
				this.executeCallback();
	
				for (var key in data.frequencies) {
					if(param === 'alpha') this.audio.src = track1
					if(param === 'beta') this.audio.src = track2
					if(param === 'delta') this.audio.src = track3
					if(param === 'theta') this.audio.src = track4
	
					if (key === param) {
						this.setState({
							title: data.frequencies[key].title,
							description: data.frequencies[key].description,
							playing: false
						})
					}
				}
			});
		} else {
			this.setState( {window: true, activeButton: param }, () => {
				this.executeCallback();
	
				for (var key in data.frequencies) {
					if(param === 'alpha') this.audio.src = track1
					if(param === 'beta') this.audio.src = track2
					if(param === 'delta') this.audio.src = track3
					if(param === 'theta') this.audio.src = track4
	
					if (key === param) {
						this.setState({
							title: data.frequencies[key].title,
							description: data.frequencies[key].description,
							playing: false
						})
					}
				}
			});
		}
	}

	startPlayer() {
		this.frameId = requestAnimationFrame(this.update);
		this.audio.play();
		this.setState({ playing: true });
	}

	update() {
		this.frameId = window.requestAnimationFrame(this.update);
		if(this.audio.currentTime) {
			let minutes = parseInt(this.audio.currentTime/60)
			let seconds = "0" + parseInt(this.audio.currentTime%60)
			let formatedSeconds = seconds.slice(-2);
			this.setState({ 
				timer: minutes + ":" + formatedSeconds,
				sliderValue: (this.audio.currentTime * 100) / this.audio.duration
			});
		} else {
			this.setState({ timer: "0:00", sliderValue: 0 });
		}
	}

	pausePlayer() {
		cancelAnimationFrame(this.frameId);
		this.audio.pause();
		this.setState({ playing: false });
	}

	managePlayer() {
		if(!this.state.playing) this.startPlayer();
		else this.pausePlayer();
	}

	manageTutorial() {
		if(this.state.tutorialStep < 2) {
			this.setState({ tutorialStep: this.state.tutorialStep + 1 })
		} else {
			this.setState({ tutorial: false, tutorialStep: 1 })
		}
	}

	searchAudio(event) {
		let target = event.currentTarget
		this.setState({ searching: true, searchValue: target.value }, () => {
			this.audio.currentTime = target.value * this.audio.duration/100
			this.setState({ searching: false })
		});
	}

	isMobile(screenWidth){
		if(window.innerHeight > window.innerWidth){
			if(screenWidth) return screenWidth <= 768;
			else return window.innerWidth <= 768;
		}
	}

	componentDidMount() {
		this.mobile = this.isMobile();
		window.addEventListener('resize', () => this.isMobile());
	}

	render() {
		return(
			<div id="interface">
				<div className={cls("overlay", {active: this.state.overlay})}>
					<p className="overlay_title"><b>ibis</b> <br/> Fones Binaurais</p>
					<p className="overlay_subtitle">Lorem ipsum subtitle here</p>
				</div>
				<div className={cls("tutorial", {active: this.state.tutorial})}>
					<div className={cls("step step1-container", {active: this.state.tutorialStep === 1})}>
						<div className="step1-img"/>
						<p className="description">Clique e arraste o botão esquerdo do mouse para movimentar a câmera.</p>
						<button className="next-step" onClick={this.manageTutorial.bind(this)}>Próximo</button>
					</div>
					<div className={cls("step step2-container", {active: this.state.tutorialStep === 2})}>
						<div className="step2-img"/>
						<p className="description">Use o scroll do mouse para aproximar ou se distanciar do fone.</p>
						<button className="next-step" onClick={this.manageTutorial.bind(this)}>Entendi</button>
					</div>
				</div>
				<button className={cls("begin", {active: this.state.overlay}, {faded: this.state.active}, {inactive: this.state.tutorial})} onClick={ this.toggleOverlay.bind(this)}>
					{this.state.overlay ? "Começar" : "Voltar"}
				</button>
				<button className={cls("showDetails", {hidden: this.state.overlay}, {inactive: this.state.tutorial})} onClick={ this.toggleDetail.bind(this)}>
					{this.state.active ? "Fechar detalhes" : "Ver detalhes"}
				</button>
				<ul className={cls("control-panel", {hidden: !this.state.active, minimized: this.state.minimized})}>
					<li>
						<button className={cls("frequency frequency-one", {hidden: !this.state.active}, {selected: this.state.activeButton === 'alpha'})} 
							onClick={ this.openWindow.bind(this, 'alpha') }>Alpha Pattern</button>
					</li>
					<li>
						<button className={cls("frequency frequency-two", {hidden: !this.state.active}, {selected: this.state.activeButton === 'beta'})} 
							onClick={ this.openWindow.bind(this, 'beta') }>Beta Pattern</button>
					</li>
					<li>
						<button className={cls("frequency frequency-three", {hidden: !this.state.active}, {selected: this.state.activeButton === 'delta'})} 
							onClick={ this.openWindow.bind(this, 'delta') }>Delta Pattern</button>
					</li>
					<li>
						<button className={cls("frequency frequency-four", {hidden: !this.state.active}, {selected: this.state.activeButton === 'theta'})} 
							onClick={ this.openWindow.bind(this, 'theta') }>Theta Pattern</button>
					</li>
				</ul>
				<div className={cls("details-panel", {active: this.state.window, minimized: this.state.minimized})}>
					<div className="details_container">
						<button className="details_close-btn" onClick={ this.toggleWindow.bind(this) }></button>
						<p className="details_title">{this.state.title}</p>
						<p className="details_description">{this.state.description}</p>
						<div className="player-container">
							<div className={cls("player-commands", { minimized: this.state.minimized })}>
								<button className={cls("play-btn", {playing: this.state.playing})} 
									onClick={this.managePlayer.bind(this)}> {this.state.playing ? "❙❙" : "►"}</button>
								<div className="time-control">
									<p className="time-elapsed">
										{this.state.timer}
									</p>
									<input type="range" value={ this.state.searching ? this.state.searchValue : this.state.sliderValue} 
										className={cls("time-slider", {playing: this.state.playing})} onChange={ this.searchAudio.bind(this) }/>
									<p className="time-duration">
										{this.audio.duration ? parseInt(this.audio.duration/60) + ":" + ("0" + parseInt(this.audio.duration%60)).slice(-2) : "0:00"}
									</p>
								</div>
							</div>
							{this.isMobile() ? <button className={cls("minimize", {mobile: this.isMobile(), minimized: this.state.minimized})} onClick={() => this.setState({minimized: !this.state.minimized})}/> : null}
						</div>
					</div>
				</div>
			</div>
		)
	}
}

Interface.protoTypes = {
	callback : PropTypes.func
};

export default Interface