const data = {
	
	frequencies: {
		alpha: {
			title: "Alpha Patterns",
			description: "Binaural beats in the alpha pattern are set at a frequency of between 8 and 13 Hz, which may encourage relaxation."
		},
		beta: {
			title: "Beta Patterns",
			description: "Binaural beats in the beta pattern are set at a frequency of between 14 Hz and 100 Hz, which may help promote concentration and alertness. However, this frequency can also increase anxiety at the higher end of the frequency range."
		},
		delta: {
			title: "Delta Patterns",
			description: "Binaural beats in the delta pattern are set at a frequency of between 0.1 and 4 Hz, which is associated with dreamless sleep."
		},
		theta: {
			title: "Theta Patterns",
			description: "Binaural beats in the theta pattern are set at a frequency of between 4 and 8 Hz, which is associated with sleep in the rapid eye movement or REM phase, meditation, and creativity."
		}
	}
}

module.exports = data